#Taller #6
#Crear una clase que implemente una calculadora aritmética.  

class Calculadora:

    numero1=0
    numero2=0

    def __init__(self, a,b):
        self.numero1 = a
        self.numero2 = b

    def sumar(self):
        suma = self.numero1 + self.numero2
        return suma

    def restar(self):
        resta = self.numero1 - self.numero2
        return resta

    def multiplicar(self):
        multiplicacion = self.numero1 * self.numero2
        return multiplicacion

    def dividir(self):
        division = self.numero1 / self.numero2
        return division

    def potenciar(self):
        potencia = self.numero1**self.numero2
        return potencia 

a = float(input("Ingrese un valor 1: "))
b = float(input("Ingrese un valor 2: "))

miCalculadora=Calculadora(a,b)

print("La suma de los valores es: ", miCalculadora.sumar())
print("La resta de los valores es: ", miCalculadora.restar())
print("La multiplicación de los valores es: ", miCalculadora.multiplicar())
print("La división de los valores es: ", miCalculadora.dividir())
print("El valor 1 elevado al valor 2 es: ", miCalculadora.potenciar())


